package base

/**
 * This class does some calculations
 */

class Calculations {

    /**
     *  receives two values and returns the sum
     *  @param a double value
     *  @param b double value
     *  @return sum of a and b
     */
    fun sum(a: Double, b: Double): Double = a+b
}

fun main(){
    println("Sum two values: 5.3 + 8.4 = ${Calculations().sum(5.3, 8.4)}")
}